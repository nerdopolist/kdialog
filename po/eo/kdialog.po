# translation of kdialog.pot to esperanto
# Copyright (C) 2007 Free Software Foundation, Inc.
# This file is distributed under the same license as the kdialog package.
# Cindy McKee <cfmckee@gmail.com>, 2007.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: kdialog\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-03-08 00:50+0000\n"
"PO-Revision-Date: 2023-01-22 23:30+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Cindy McKee,Oliver Kellogg"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "cfmckee@gmail.com,okellogg@users.sourceforge.net"

#: kdialog.cpp:265
#, kde-format
msgid "KDialog"
msgstr "KDialog"

#: kdialog.cpp:266
#, kde-format
msgid "KDialog can be used to show nice dialog boxes from shell scripts"
msgstr ""
"KDialog povas esti uzata por montri belajn dialogujojn el ŝelaj skriptoj"

#: kdialog.cpp:268
#, kde-format
msgid "(C) 2000, Nick Thompson"
msgstr "(C) 2000, Nick Thompson"

#: kdialog.cpp:269
#, kde-format
msgid "David Faure"
msgstr "David Faure"

#: kdialog.cpp:269
#, kde-format
msgid "Current maintainer"
msgstr "Nuna prizorganto"

#: kdialog.cpp:270
#, kde-format
msgid "Brad Hards"
msgstr "Brad Hards"

#: kdialog.cpp:271
#, kde-format
msgid "Nick Thompson"
msgstr "Nick Thompson"

#: kdialog.cpp:272
#, kde-format
msgid "Matthias Hölzer"
msgstr "Matthias Hölzer"

#: kdialog.cpp:273
#, kde-format
msgid "David Gümbel"
msgstr "David Gümbel"

#: kdialog.cpp:274
#, kde-format
msgid "Richard Moore"
msgstr "Richard Moore"

#: kdialog.cpp:275
#, kde-format
msgid "Dawit Alemayehu"
msgstr "Dawit Alemayehu"

#: kdialog.cpp:276
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: kdialog.cpp:282
#, kde-format
msgid "Question message box with yes/no buttons"
msgstr "Demanda mesaĝkesto kun jes/ne butonoj"

#: kdialog.cpp:283
#, kde-format
msgid "Question message box with yes/no/cancel buttons"
msgstr "Demanda mesaĝkesto kun jes/ne/nuligi butonoj"

#: kdialog.cpp:284
#, kde-format
msgid "Warning message box with yes/no buttons"
msgstr "Averta mesaĝkesto kun jes/ne butonoj"

#: kdialog.cpp:285
#, kde-format
msgid "Warning message box with continue/cancel buttons"
msgstr "Averta mesaĝkesto kun daŭrigi/nuligi butonoj"

#: kdialog.cpp:286
#, kde-format
msgid "Warning message box with yes/no/cancel buttons"
msgstr "Averta mesaĝo kun butonoj jes/ne/nuligi"

#: kdialog.cpp:288
#, kde-format
msgid "Use text as OK button label"
msgstr "Uzi tekston por OK butonetikedo"

#: kdialog.cpp:289
#, kde-format
msgid "Use text as Yes button label"
msgstr "Uzi tekston por Jes butonetikedo"

#: kdialog.cpp:290
#, kde-format
msgid "Use text as No button label"
msgstr "Uzi tekston por Neniu butonetikedo"

#: kdialog.cpp:291
#, kde-format
msgid "Use text as Cancel button label"
msgstr "Uzi tekston por Nuligi butonetikedon"

#: kdialog.cpp:292
#, kde-format
msgid "Use text as Continue button label"
msgstr "Uzi tekston kiel Daŭrigi butonetikedon"

#: kdialog.cpp:293
#, kde-format
msgid "'Sorry' message box"
msgstr "'Pardonu' mesaĝkesto"

#: kdialog.cpp:294
#, kde-format
msgid "'Sorry' message box with expandable Details field"
msgstr "'Pardonu' mesaĝkesto kun vastigebla kampo Detaloj"

#: kdialog.cpp:295
#, kde-format
msgid "'Error' message box"
msgstr "'Eraro' mesaĝkesto"

#: kdialog.cpp:296
#, kde-format
msgid "'Error' message box with expandable Details field"
msgstr "'Eraro' mesaĝkesto kun vastigebla kampo Detaloj"

#: kdialog.cpp:297
#, kde-format
msgid "Message Box dialog"
msgstr "Mesaĝkesta dialogujo"

#: kdialog.cpp:298
#, kde-format
msgid "Input Box dialog"
msgstr "Enmeta dialogujo"

#: kdialog.cpp:299
#, kde-format
msgid "Image Box dialog"
msgstr "Bildkesta Dialogujo"

#: kdialog.cpp:300
#, kde-format
msgid "Image Box Input dialog"
msgstr "Bildkest-Enmeta Dialogujo"

#: kdialog.cpp:301
#, kde-format
msgid "Password dialog"
msgstr "Pasvorta dialogujo"

#: kdialog.cpp:302
#, kde-format
msgid "New Password dialog"
msgstr "Novpasvorta dialogujo"

#: kdialog.cpp:303
#, kde-format
msgid "Text Box dialog"
msgstr "Tekstokesta dialogujo"

#: kdialog.cpp:304
#, kde-format
msgid "Text Input Box dialog"
msgstr "Tekstenmeta dialogujo"

#: kdialog.cpp:305
#, kde-format
msgid "ComboBox dialog"
msgstr "KomboBox-Dialogujo"

#: kdialog.cpp:306
#, kde-format
msgid "Menu dialog"
msgstr "Menua dialogujo"

#: kdialog.cpp:307
#, kde-format
msgid "Check List dialog"
msgstr "Markolista dialogujo"

#: kdialog.cpp:308
#, kde-format
msgid "Radio List dialog"
msgstr "Radiolista dialogujo"

#: kdialog.cpp:309
#, kde-format
msgid "Passive Popup"
msgstr "Senfokusa ŝpruc-dialogujo"

#: kdialog.cpp:310
#, kde-format
msgid "Popup icon"
msgstr "Ŝprucfenestra piktogramo"

#: kdialog.cpp:311
#, kde-format
msgid "File dialog to open an existing file (arguments [startDir] [filter])"
msgstr ""
"Dosiera dialogo por malfermi ekzistantan dosieron (argumentoj [startDir] "
"[filtrilo])"

#: kdialog.cpp:312
#, kde-format
msgid "File dialog to save a file (arguments [startDir] [filter])"
msgstr ""
"Dosierdialogujo por konservi dosieron (argumentoj [startDir] [filtrilo])"

#: kdialog.cpp:313
#, kde-format
msgid "File dialog to select an existing directory (arguments [startDir])"
msgstr ""
"Dosierdialogujo por elekti ekzistantan dosierujon (argumentoj [startDir])"

#: kdialog.cpp:314
#, kde-format
msgid "File dialog to open an existing URL (arguments [startDir] [filter])"
msgstr ""
"Dosierdialogujo por malfermi ekzistantan URL (argumentoj [startDir] "
"[filtrilo])"

#: kdialog.cpp:315
#, kde-format
msgid "File dialog to save a URL (arguments [startDir] [filter])"
msgstr "Dosierdialogujo por konservi URL (argumentoj [startDir] [filtrilo])"

#: kdialog.cpp:316
#, kde-format
msgid "Icon chooser dialog (arguments [group] [context])"
msgstr "Piktogramelektila dialogujo (argumentoj [grupo] [kunteksto])"

#: kdialog.cpp:317 kdialog_progress_helper.cpp:40
#, kde-format
msgid "Progress bar dialog, returns a D-Bus reference for communication"
msgstr "Plenumindikila dialogujo, redonas referencon de D-Bus por komunikado"

#: kdialog.cpp:318
#, kde-format
msgid "Color dialog to select a color"
msgstr "Kolordialogujo por elekti koloron"

#: kdialog.cpp:319
#, kde-format
msgid "Allow --getcolor to specify output format"
msgstr "Permesi al --getcolor specifi eligformaton"

#: kdialog.cpp:321 kdialog_progress_helper.cpp:41
#, kde-format
msgid "Dialog title"
msgstr "Dialoga titolo"

#: kdialog.cpp:322
#, kde-format
msgid "Default entry to use for combobox, menu, color, and calendar"
msgstr "Defaŭlta enigero uzenda por kombokesto, menuo, koloro kaj kalendaro"

#: kdialog.cpp:323
#, kde-format
msgid ""
"Allows the --getopenurl and --getopenfilename options to return multiple "
"files"
msgstr ""
"Permesas al la opcioj --getopenurl kaj --getopenfilename redoni plurajn "
"dosierojn"

#: kdialog.cpp:324
#, kde-format
msgid ""
"Return list items on separate lines (for checklist option and file open with "
"--multiple)"
msgstr ""
"Liveri listajn erojn sur apartaj linioj (por marklistaj elektoj kaj dosier-"
"malfermado kun parametro --multiple)"

#: kdialog.cpp:325
#, kde-format
msgid "Outputs the winId of each dialog"
msgstr "Liveras la fenestran identigilon (winId) por ĉiu dialogo"

#: kdialog.cpp:326
#, kde-format
msgid ""
"Config file and option name for saving the \"do-not-show/ask-again\" state"
msgstr ""
"Agorda dosiero kaj opcionomo por konservi la staton \"ne-montri/demandi "
"denove\""

#: kdialog.cpp:328
#, kde-format
msgid "Slider dialog box, returns selected value"
msgstr "Ŝovila dialogujo, liveras elektitan valoron"

#: kdialog.cpp:329
#, kde-format
msgid ""
"Date format for calendar result and/or default value (Qt-style); defaults to "
"'ddd MMM d yyyy'"
msgstr ""
"Datformato por kalendarrezulto kaj/aŭ defaŭlta valoro (Qt-stilo); defaŭlte "
"al 'ddd MMM d jyyy'"

#: kdialog.cpp:330
#, kde-format
msgid "Calendar dialog box, returns selected date"
msgstr "Kalendara dialogujo, liveras elektitan daton"

#: kdialog.cpp:333
#, kde-format
msgid "Makes the dialog transient for an X app specified by winid"
msgstr ""
"Igas la dialogujon pasema por X-aplikaĵo difinita per winid (fenestra "
"identigilo)"

#: kdialog.cpp:334
#, kde-format
msgid "A synonym for --attach"
msgstr "Sinonimo por --attach"

#: kdialog.cpp:335
#, kde-format
msgid "Dialog geometry: [=][<width>{xX}<height>][{+-}<xoffset>{+-}<yoffset>]"
msgstr "Dialogogeometrio: [=][<width>{xX}<height>][{+-}<xoffset>{+-}<yoffset>]"

#: kdialog.cpp:337 kdialog_progress_helper.cpp:42
#, kde-format
msgid "Arguments - depending on main option"
msgstr "Argumentoj - depende de ĉefa opcio"

#: kdialog.cpp:516
#, kde-format
msgid "Do not ask again"
msgstr "Ne demandi denove"

#: kdialog.cpp:716
#, kde-format
msgid "Syntax: --combobox <text> item [item] ..."
msgstr "Sintakso: --combobox <teksto> ero [ero] ..."

#: kdialog.cpp:738
#, kde-format
msgid "Syntax: --menu text tag item [tag item] ..."
msgstr "Sintakso: --menu teksto etikedo [etikedo] ..."

#: kdialog.cpp:762
#, kde-format
msgid "Syntax: --checklist text tag item on/off [tag item on/off] ..."
msgstr ""
"Sintakso: --checklist teksto etikedo-ŝaltita/malŝaltita [etikedo-ŝaltita/"
"malŝaltita] ..."

#: kdialog.cpp:780
#, kde-format
msgid "Syntax: --radiolist text tag item on/off [tag item on/off] ..."
msgstr ""

#: kdialog.cpp:811
#, kde-format
msgctxt "@title:window"
msgid "Open"
msgstr "Malfermi"

#: kdialog.cpp:863
#, kde-format
msgctxt "@title:window"
msgid "Save As"
msgstr "Konservi kiel"

#: kdialog.cpp:897
#, kde-format
msgctxt "@title:window"
msgid "Select Directory"
msgstr "Elekti Dosierujon"

#: kdialog.cpp:1013
#, kde-format
msgctxt "@title:window"
msgid "Choose Color"
msgstr "Elekti Koloron"

#: progressdialog.cpp:24
#, kde-format
msgid "Cancel"
msgstr "Nuligi"

#: widgets.cpp:107 widgets.cpp:141 widgets.cpp:154
#, kde-format
msgid "kdialog: could not open file %1"
msgstr "kdialog: ne povis malfermi la dosieron %1"
